This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Quick summary
Initially I had it set up with an individual cell that made the grid and the cell had either an active or inactive state. I thought about this and realised it isn't very extensible, so I've refactored the grid. 

I considered using the canvas to draw based on where you moved the mouse, but without the ability to undo actions then this didn't really work. 

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


