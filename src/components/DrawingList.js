import React, { Component } from 'react';

export default class DrawingList extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  renderList() {
    let patternList = this.props.patterns.map(pattern => {
      return <li onClick={() => this.props.updatePattern(pattern.title)}>{pattern.title}</li>;
    });
    return patternList;
  }

  render() {
    return (
      <ul>
        {this.renderList()}
      </ul>
    );
  }
}
