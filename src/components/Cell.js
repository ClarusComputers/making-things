import React, { Component } from 'react';

export default class Cell extends Component {
  constructor() {
    super();
    this.state = {
      clicked: false,
    };
  }

  cellClicked(x, y) {
    console.log(x, y)
  }

  render() {
    return (
      <div className='grid-cell' onClick={() => this.cellClicked(this.props.x, this.props.y)}>{this.props.symbol}</div>
    );
  }
}
