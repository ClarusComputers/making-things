import React, { Component } from 'react';
import './App.css';
import Canvas from './components/Canvas';
import DrawingList from './components/DrawingList';

class App extends Component {

  constructor() {
    super();
    this.updatePattern = this.updatePattern.bind(this);
  }
  
state = {
  patterns: [
    {
      active: false,
      title: 'Person',
      cells: [
        {x: 1, y: 10},
        {x: 2, y: 8},
        {x: 3, y: 7},
        {x: 4, y: 4},
      ]
  },
  {
    active: false,
    title: 'Flower',
    cells: [
      {x: 1, y: 10},
      {x: 2, y: 8},
      {x: 3, y: 7},
      {x: 4, y: 4},
    ]
},
{
  active: false,
  title: 'House',
  cells: [
    {x: 1, y: 10},
    {x: 2, y: 8},
    {x: 3, y: 7},
    {x: 4, y: 4},
    {x: 8, y: 10},
    {x: 7, y: 8},
    {x: 4, y: 10},
    {x: 5, y: 10},
  ]
}
]
}

  renderActiveCanvas() {
    const canvas = this.state.patterns.map(pattern => {
      if (pattern.active === true) {
        return <Canvas {...pattern} />;
      }
      return null;
    })
    return canvas;
  }

  addPattern() {
    const pattern = {
      title: 'New pattern',
      active: true,
      cells: [],
    }

    this.updatePattern();

    this.setState({patterns: [...this.state.patterns, pattern]});
  }

  updatePattern(pattern) {
    const patterns = this.state.patterns;
    patterns.forEach(singlePattern => {
      singlePattern.active = false;
      if (singlePattern.title === pattern) {
        singlePattern.active = true;
      }
    });
    this.setState({patterns});
  }

  render() {
    return (
      <div className="app">
        <div className="col-left">
          <h2>My pattern list</h2>
          <button className="pattern-btn" onClick={this.addPattern.bind(this)}>Add pattern</button>
          <DrawingList updatePattern={this.updatePattern} patterns={this.state.patterns} />
        </div>
        <div className="col-right">
          {this.renderActiveCanvas()}
        </div>
      </div>
    );
  }
}

export default App;
